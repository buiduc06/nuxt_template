let _ = require('lodash')

let BasicRouter = require('./basic').router()

module.exports = {
  arrayRoute () {
    return _.concat(BasicRouter)
  }
}
