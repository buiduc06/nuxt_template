const BasicRouter = [
  {
    path: '/',
    component: ('~/views/home/index'),
    name: 'home'
  }
]

module.exports = {
  router () {
    return BasicRouter
  }
}
