const webpack = require('webpack')
const router = require('./router/index')

module.exports = {

  dev: (process.env.NODE_ENV !== 'production'),

  watchers: {
    chokidar: {
      usePolling: true
    },
    webpack: {
      aggregateTimeout: 1000,
      poll: 1000
    }
  },

  /**
   * Let you define all default meta for your application inside
   */
  head: {
    title: 'nuxtjs app',
    meta: [
      { 'http-equiv': 'Content-Type', content: 'text/html; charset=utf-8' },
      { 'http-equiv': 'Content-Language', content: 'vi' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /**
   * Uses its own component to show a progress bar between the routes.
   * You can customize it, disable it or create your own component.
   */
  loading: {
    color: 'white',
    height: '2px'
  },

  /**
   * Use lru-cache to allow cached components for better render performances
   * */
  render: {
    bundleRenderer: {
      cache: require('lru-cache')({
        max: 10000,
        maxAge: 1000 * 60 * 15
      })
    }
  },

  /**
   * Build configuration
   */
  build: {
    /**
     * Use ssr option to customize vue SSR bundle renderer
     */
    ssr: {
      runInNewContext: false
    },
    /*
   ** Run ESLINT on save
   */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },

    plugins: [
      // Automatically load plugin instead of having to import or require them everywhere.
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],

    // Lets you add modules inside the vendor.bundle.js file generated to reduce the size of the app bundle.
    // It's really useful when using external modules.
    vendor: ['axios', 'vue-cookie']
  },

  modules: [
    '@nuxtjs/optimize',
    '@nuxtjs/offline',
    ['@nuxtjs/google-analytics', { ua: 'UA-101641104-1' }]
  ],

  /**
   * Lets you define the CSS you want to set globally
   * */
  css: [
    { src: '~/assets/scss/style.scss', lang: 'scss' }
  ],

  // The plugins property lets you add vue.js plugins easily to your main application. vue-google-maps.js
  plugins: [
    { src: '~/plugins/components.js', ssr: true },
    { src: '~/plugins/carousel.js', ssr: false }
  ],

  // Nuxt.js lets you create environment variables that will be shared for the client and server-side.
  env: {
    DOMAIN: 'http://localhost:3000',
    TOKEN_AUTH: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbXdhcGkuZGV2L2F1dGgvbG9naW4iLCJpYXQiOjE0OTI0Mzk0NjcsImV4cCI6MzMwMjg0Mzk0NjcsIm5iZiI6MTQ5MjQzOTQ2NywianRpIjoicms3VlVGbm9hVDFva3FhVCIsInN1YiI6MX0.NxQeB5B5VVKNmGTE1Tbj_tZl5231h8QeUTe8',
    API_DOMAIN: 'http://27.118.16.39:8080',
    NODE_ENV: 'local',
    HOST: '127.0.0.1',
    PORT: '3000',
    SECRET_KEY: 'ARWIFWFLWFIQOFKWFI78@$ORJJFOTJTJ'
  },

  router: {
    // middleware: 'anonymous',

    extendRoutes (routes, resolve) {
      let arrayRoute = router.arrayRoute(resolve)
      arrayRoute.map((item) => routes.push(item))
    }
  }
}
