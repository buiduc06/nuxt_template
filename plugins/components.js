import Vue from 'vue'
import components from '../components/index.js'

Object.keys(components).forEach(function (key) {
  let component = components[key]
  Vue.component(component.name, component)
})
