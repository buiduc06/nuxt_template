export default {
  GET_CART_DATA (state) {
    return state.shopingCart
  },
  GET_COUNT_DATA (state) {
    return state.shopingCart ? state.shopingCart.length : 0
  },
  GET_TOTAL_PRICE (state) {
    return state.shopingCart ? state.shopingCart.map(item => {
      return item.id || null
    }) : 0
  }
}
