import Vue from 'vue'

export default {
  SET_DATA_CART (state, data) {
    Vue.set(state, 'shopingCart', data || null)
  }
}
