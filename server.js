const app = require('express')()
const {Nuxt, Builder} = require('nuxt')
const schedule = require('node-schedule')
const axios = require('axios')

require('dotenv').config()

// var rootCas = require('ssl-root-cas/latest').create()

// rootCas.addFile(__dirname + '/config/ssl/api_mywork_com_vn_ca.crt')

// will work with all https requests will all libraries (i.e. request.js)
// require('https').globalAgent.options.ca = rootCas

const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 8969

// Import and set Nuxt.js options
let config = require('./nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

const nuxt = new Nuxt(config)

// Start build process in dev mode
if (config.dev) {
  console.log('Go here is DEV')
  const builder = new Builder(nuxt)
  builder.build()
}

if (process.env.APP_ENV === 'production') {
  // Cache Homepage
  schedule.scheduleJob('*/30 * * * * *', function () {
    axios.get(`${process.env.APP_DOMAIN}?force_cache=1`).then((res) => {
      console.log(`Cached Homepage`)
    }).catch((err) => {
      console.log(err)
    })
  })

  // Cache Listing
  schedule.scheduleJob('*/30 * * * * *', function () {
    axios.get(`${process.env.APP_DOMAIN}/tuyen-dung?force_cache=1`).then((res) => {
      console.log(`Cached Listing Page`)
    }).catch((err) => {
      console.log(err)
    })
  })
}

// Give nuxt middleware to express
app.use(nuxt.render)

// Start express server
app.listen(port, host)
console.log('Server listening on ' + host + ':' + port)
